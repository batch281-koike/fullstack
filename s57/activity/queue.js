let collection = [];

// Write the queue functions below.

function print() {
    let result = [];
    for (let i = 0; i < collection.length; i++) {
        
    }
    return result;
}

function enqueue(element) {
    //In this function you are going to make an algo that will add an element to the array (last element)
    collection[collection.length] = element;
    return collection;
}


function dequeue() {
    // In here you are going to remove the first element in the array
    for(let i=0; i<collection.length-1; i++){
        collection[i]=collection[i+1];
    }
    collection.length--;
    return collection;
}




function front() {
    // you will get the first element
    if (collection.length === 0) {
      return undefined;
    }

    return collection[0];
}



function size() {
     // Number of elements
    let counter = 0;
    let i = 0;

    while (collection[i] !== null && collection[i] !== undefined) {
        counter++;
        i++;
    }
    return counter;
}

function isEmpty() {
    //it will check whether the function is empty or not
    let empty = true;

    for (let key in collection) {
        empty = false;
        break;
    }

    return empty;
}


module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};